﻿using EAAutoFramework.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Diagnostics;

namespace EAAutoFramework.Extensions
{
    public static class WebDriverExtensions
    {
        internal static void WaitForDocumentLoaded(this IWebDriver driver)
        {
            driver.WaitForCondition(drv =>
            {
                string state = drv.ExecuteJavaScript<string>("return document.readyState");
                return state == "complete";
            }, Settings.Timeout);
        }

        internal static void WaitForCondition<T>(this T obj, Func<T, bool> condition, int timeOut)
        {
            Func<T, bool> execute =
                (arg) =>
                {
                    try
                    {
                        return condition(arg);
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                };

            var sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds < timeOut)
            {
                if (execute(obj))
                {
                    break;
                }
            }
        }

    }
}
