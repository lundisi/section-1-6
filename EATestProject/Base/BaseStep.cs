﻿using EAAutoFramework.Config;
using EAAutoFramework.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAAutoFramework.Base
{
    public abstract class BaseStep : Base
    {
        public virtual void NaviateSite()
        {
            DriverContext.Browsers.GoToURL(Settings.AUT);
            LogHelpers.Write("Opened the browser !!!");
        }

    }
}
